## Beauty of alternate priorities

### Chris Uzdavinis

-next-

#### This code is designed to be fast
<pre>
<code>
float Q_rsqrt( float number )
{
	long i;
	float x2, y;
	const float threehalfs = 1.5F;

	x2 = number * 0.5F;
	y  = number;
	i  = * ( long * ) &y; // evil floating point bit level hacking
	i  = 0x5f3759df - ( i >> 1 );  // what the f***?
	y  = * ( float * ) &i;
	y  = y * ( threehalfs - ( x2 * y * y ) );   // 1st iteration
	return y;
}
</code
</pre>
(from Quake III)

-next-

#### This code is designed to be obscure

![](images/obscure.png)
(by Yusuke Endoh)

https://godbolt.org/z/zjS2kx


-next-


#### This code is designed to be safe at trust boundaries

<pre>
<code>
struct Data {double a; char b; double c; };

bool safeCopy(uint8_t * buf, size_t sz, Data const & arg) {
    ...
   size_t pos = 0;
   memcpy(buf + pos, &arg.a, sizeof(arg.a)); pos += sizeof(arg.a);
   memcpy(buf + pos, &arg.b, sizeof(arg.b)); pos += sizeof(arg.b);
   memcpy(buf + pos, &arg.c, sizeof(arg.c)); pos += sizeof(arg.c);
   return true;
}

</code>
</pre>

###### Prevent padding bytes from escaping


-next-

### What's important to you?

-next-


### BEAUTIFUL CODE

* Code that does what it is supposed to do
* Resonates with a human in some way
* Is designed and implemented in accordence with what was priorized

-next-

PROBLEM:

#### Given a flat vector of points stored as alternating x, y elements,

#### Find the greatest horizontal _or_ vertical distance between any 2 points.


Code snippet:

<pre>
<code>
int largestDistance(std::vector&lt;int> const & points) {
}
</code>
</pre>

-next-
#### Here is a basic solution
<pre>
<code>
int largestDistance(std::vector&lt;int> const & vec) {
    int maxDist = 0;
    for (size_t i = 0; i &lt; size(vec); ++i) {
        for (size_t j = 0; j &lt; size(vec); j++) {
            if (j % 2 == i % 2) {
                maxDist = max(maxDist, abs(vec[i]-vec[j]));
            }
        }
    }
    return maxDist;
}
</code>
</pre>

192 bytes
-next-

### Change Priority:

#### <span style="color:purple"> Small Source Code </span>

If _smallness_ is beauty, this code is _ugly_.


-next-

* "meaningful" 1-letter variable names
* arg: don't use std:: qualifier, copy by value
* size_t -> int

<pre>
<code>
  int largestDistance(vector&lt;int> p) {
      int m = 0;
      for (int i = 0; i < size(p); ++i) {
          for (int j = 0; j < size(p); ++j) {
              if (j % 2 == i % 2) {
                  m = max(m, abs(p[i] - p[j]));
              }
          }
      }
      return m;
  }
</code>
</pre>
141 bytes (was 192)

_Can we do something about the loops?_
-next-

* use ranged for loops
* i,j: manual indicies
* don't need abs()
* incr j in test
<pre>
<code>
int largestDistance(vector&lt;int> p) {
    int i = 0, j, m = 0;
    for (int a : p) {
        ++i;
        j = 0;
        for (int b : p) {
            if (++j % 2 == i % 2)
                m = max(m, a-b);
        }
    }
    return m;
}
</code>
</pre>
114 bytes (was 141)
Replace the IF next...

-next-

* "if" becomes a ternary expression
* remove {  } around inner for loop body
<pre>
<code>
int largestDistance(vector&lt;int> p) {
    int i = 0, j, m = 0;
    for (int a : p) {
      //for (int b : p) {
      //    ++j;
      //    if (j % 2 == i % 2) {
      //        m = max(m, a-b);
      //    }
      //}
        ++i; j = 0;
        for (int b : p)
            ++j % 2 ^ i % 2 ?: m = max(m, a-b);
    }
    return m;
}

</code>
</pre>

110 bytes (was 114)
_Can we get rid of {} around for loop?_
-next-
### Aside
The comma operator creates compound expressions, saving curly braces

<pre>
<code>
  { ++y; i = 0; a = 3; }
  // vs
  ++y, i = 0, a = 3;

</code>
</pre>

-next-
* range expression becomes compound comma-operator expression
* drop outer loop curly braces
<pre>
<code>
int largestDistance(vector&lt;int> p) {
    int i = 0, j, m = 0;
    for (int a : p)
//  {
//    ++i; j = 0;
//    for (int b : p)
//        ++j % 2 != i % 2 ?: m = max(m, a-b);
//  }
        for (int b : ++i, j=0, p)
            ++j % 2 ^ i % 2 ?: m = max(m, a-b);
    return m;
}
</code>
</pre>
108 bytes (was 110) _Do we really need i and j?_

-next-
* take reference to elements
* drop i, j
* test: distance between 2 element addresses is even
<pre>
<code>
int largestDistance(vector&lt;int> p) {
    int m = 0;
    for (int& a : p)
        for (int& b : p)
            (&b - &a) % 2 ?: m = max(m, a- b);
    return m;
}
</code>
</pre>
97 bytes (was 108)
_anything else?_
-next-

* Improve test with lower precedence operator & 1
* drop max, merge logic into test

<pre>
<code>
int largestDistance(vector&lt;int> p) {
    int m = 0;
    for (int& a : p)
        for (int& b : p)
            &b - &a & 1 | m > b-a ?: m = b-a;
    return m;
}
</code>
</pre>
94 bytes (was 97, started at 192)

BEAUTIFUL!

-next-

### Math tricks:

| Orig       | Replace    | Saves   | Notes       |
|------------|------------|---------|-------------|
| a != b     | a^b _or_ a-b  | 1 byte  | lower prec  |
| a >= b     | a/b        | 1 byte  | b > 0       |
| (a+1) % 10 | -~a % 10   | 2 bytes | higher prec |
| (a-1) % 10 | ~-a % 10   | 2 bytes | "           |
| a>b && b>c | a>b & b>c  | 1 byte  | booleans    |



-next-

#### Fin

(that's short for "the end")
